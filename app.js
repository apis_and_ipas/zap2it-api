var express     = require('express'),
    morgan      = require('morgan'),
    compression = require('compression'),
    debug       = require('debug')('zap2it'),
    router      = require('./src/router'),
    middleware  = require('./src/middleware');

var redis     = require('redis');
var Promise   = require('bluebird');
var app;

module.exports = function(options, callback){
    debug("application starting...");

    // express setup
    app = express();

    // set app config keys
    // env vars are ALWAYS strings!
    app.set('api_key', process.env.API_KEY);
    app.set('auth_token', process.env.AUTH_TOKEN);

    // set up app-wide redis instance
    var cache = Promise.promisifyAll(redis.createClient());
    app.set('cache', cache);

    // add middlewares
    app.use(compression());
 
    // homegrown middleware
    app.use(middleware.cors());
    app.use(middleware.responseHelpers());

    if (options.quiet !== true) {
        app.use( morgan('dev') );
    }

    // magic starts here
    app.use('/api', router(options));

    app.listen(options.port);

    // expose app for global instances / configuration
    exports.app = app; 

    if (typeof callback === 'function') {
        callback();
    }
};

