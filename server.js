var app     = require('./app'),
    debug   = require('debug')('zap2it:server'),
    cluster = require('cluster'),
    os      = require('os');

// load env vars
require('dotenv').load();

// Count the machine's CPUs
var WORKERS =  process.env.WORKERS || os.cpus().length;


// Server code for master process
if (cluster.isMaster) {

    // Create a worker for each CPU
    for (var i = 0; i < WORKERS; i++) {
        cluster.fork();
    }

    // respawn
    cluster.on('exit', function (worker, code, signal) {
        debug('worker', worker.process.pid, 'died :(');
        debug('spawning a new worker');
        cluster.fork();
    })

} else {
 

    var appOptions = {
        port:  process.env.PORT  || "4040",
        redisPort: process.env.REDIS_PORT,
        redisHost: process.env.REDIS_HOST,
        quiet: process.env.QUIET || false
    };

    app(appOptions, function () {
        debug('Server now running on localhost:%d', appOptions.port);
        debug('spawning worker #' + cluster.worker.id);
    });
}
