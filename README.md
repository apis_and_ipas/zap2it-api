# Zap2It Grid API

## Setup

1. Copy sample.env to .env
2. Install node.js/npm and redis-server
3. Start redis-server
4. Run `npm install`
5. Run `npm run start`

## Server Requirements

- UbuntuLTS + Nginx
- Nginx configured as reverse proxy w/ gzip
- `redis-server` running on default configuration

## API Endpoints

### Lineup Provider Search

```
GET /api/v1/lineups
```

Query Params:

- `postalCode` - ZIP or postal code
- `countryCode` - 3-letter country code

### Grid Data

```
GET /api/v1/lineup/grid
```

Query Params:
 
- `time` - unix timestamp - starting time for displayed grid data  
- `lineupId` - The cable provide to which the grid data applies.

When a lineup's grid data is requested, a 3-hour block of programming (a 'page') is 
fetched and cached. Each page is cached on the client's sessionStorage, which is deleted whenever a 
tab is closed. As the infinite scroll functionality triggers the fetching of 
the next chunk, it's data is appended to the sessionStorage 'page'. Currently, 
and not by design, only the first chunk of each page is saved to sessionStorage.


## Docker Image

First, you need to install [boot2docker](http://boot2docker.io/) with homebrew:

```
brew install boot2docker
```

Make sure to load boot2docker into current session:

```
$(boot2docker shellinit)
```

Then, init and start VM:

```
boot2docker init
boot2docker up
```

In the current directory run:

```
make docker
```

Build process from scratch could take some time (usually around 5-10mins), but 
each next build will use docker cache and will take around 10-15 seconds.

To execute image locally run:

```
docker run --rm -i -p 5000:5000 -e API_KEY=foobar -e PORT=5000 -e WORKERS=1 doejo/zap2it
```

Then open url in your browser:

```
open http://$(boot2docker ip):5000/
```

To push image to docker registry, run command:

```
make docker-release
```

## Deployment

Deploy with the following command:

```
make deploy
```