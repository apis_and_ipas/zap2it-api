IMAGE="doejo/zap2it"

test:
	@echo "Nothing to test"

.PHONY: docker
docker:
	@docker pull $(IMAGE) || true
	docker build -t $(IMAGE) .

.PHONY: docker-test
docker-test:
	docker run -i -t $(IMAGE) bash

.PHONY: docker-release
docker-release:
	docker push $(IMAGE)

.PHONY: deploy
deploy: docker docker-release
	ssh root@104.130.73.26 /root/start.sh