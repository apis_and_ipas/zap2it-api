'use strict';

module.exports = function(){

    return function(req, res, next) {

        var api_token = req.query.api_token || req.headers['X-Auth-Token'];
        var auth_token = req.app.get('auth_token');

        if (!api_token) {

            // Auth Token not present ...
            res.badRequest("API token required");

        } else if (api_token != auth_token) {

            // Auth Token not valid ...
            res.unauthorized();

        } else {
            next();
        }


    }
};