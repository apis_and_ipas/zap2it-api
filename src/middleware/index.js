'use strict';

var changeCase = require('change-case');

// Export the entire directory as a single module
module.exports = require('require-directory')(module, {rename: function (name) {
    return changeCase.camelCase(name);
}});