var Gracenote = require('node-tms-onconnect');
var api = new Gracenote(process.env.API_KEY);
var _ = require('lodash');
var Promise = require('bluebird');

var showsData = require('../shared/showsData'); 

module.exports = function (req, res) {

    // get list of show Ids as array
    var showsIds = req.query.showsIds ? req.query.showsIds.split(',') : null;
    var lineupId = req.query.lineupId ? req.query.lineupId : null;
 
    if (!lineupId) {
        res.badRequest('You must pass a lineup id');
        return;
    }

    if (!showsIds || !showsIds.length > 0) {
        res.badRequest('You must pass at least one show id');
        return;
    }  

    Promise.map(showsIds, function(showId) {
        return showsData.fetchAirings(showId, { lineupId: lineupId });
    })
    .then(function(data) {
        console.log('Promise.map data', data)
        res.ok(data)
    })
    .catch(console.log.bind(console));

}