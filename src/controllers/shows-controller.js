var showData = require('../shared/showsData');

module.exports = {

    details: function(req, res) {

        var tmsId = req.params.tmsId;
 
        showData
            .fetchDetails(tmsId)
            .then(res.ok);
    },

    airings: function(req, res) {

        var seriesId = req.params.seriesId;

        var payload = {
            lineupId: req.query.lineupId,
            startDateTime: req.query.startTime
        }

        // capture the startTime if passed
        if (req.query.startTime) {
            payload.startDateTime = req.query.startTime;
        }
        
        //capture endtime if passed
        if (req.query.endTime) {
            payload.endDateTime = req.query.endTime;
        }

        showData
            .fetchAirings(seriesId, payload)
            .then(res.ok);
    }

};