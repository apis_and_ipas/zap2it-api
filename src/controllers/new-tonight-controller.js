var Gracenote = require('node-tms-onconnect');
var moment = require('moment');
var _ = require('lodash');
var debug = require('debug')('zap2it');
var api = new Gracenote(process.env.API_KEY);
var timestampRounder = require('../shared/round-unix-timestamp');


var startAtPrimetime = function(startTime, shows) {
    return _.filter(shows, function(show) {
        return moment(show.startTime).unix() >= startTime;
    });
};

var transformShows =  function(shows) {
    return shows.map(function(show) {
        
        var timestamp = moment(show.startTime).unix()
        var timeslot = timestampRounder.round(timestamp);

        return { 
            // station: {
            //     channel: show.station.channel,
            //     callSign: show.station.callSign,
            //     logo: "http://zap2it.tmsimg.com/" + show.station.preferredImage.uri + "?w=55"
            // },
            channel: show.station.channel,
            callSign: show.station.callSign,
            logo: "http://zap2it.tmsimg.com/" + show.station.preferredImage.uri + "?w=55",
            timestamp: timestamp,
            timeslot: timeslot,
            id: show.program.tmsId,
            seriesId: show.program.seriesId,
            title: show.program.title,
            duration: show.duration,
            startTime: show.startTime,
            endTime: show.endTime,
            desc: show.program.shortDescription,
            season: show.program.seasonNum || null,
            releaseYear: show.program.releaseYear || null,
            episode: show.program.episode || null,
            episodeTitle: show.program.episodeTitle || null,
            thumbnail: "http://zap2it.tmsimg.com/" + show.program.preferredImage.uri + "?w=125",
            flags: (function () {

                if ( _.includes( show.qualifiers, "Live" ) ) {
                    return "Live";
                }

                if ( _.includes( show.qualifiers, "New" ) ) {
                    return "New";
                }

            })(),
            tags: (function () {
                var tags = [];

                if ( _.includes( show.qualifiers, "Stereo" ) ) {
                    tags.push("Stereo");
                }

                if ( _.includes( show.qualifiers, "CC" ) ) {
                    tags.push("CC") ;
                }

                return tags;
            })(),
            rating: (function () {
                // return desired formatting for rating
                if (show.ratings && show.ratings[0] && show.ratings[0].code) {
                    switch (show.ratings[0].code) {
                       case 'TVY':
                       case 'TVY7':
                       case 'TVY7FV':
                               return 'TV-Y';
                       case 'TVG':
                           return 'TV-G';
                           break;
                       case 'TVPG':
                           return 'TV-PG';
                           break;
                       case 'TV14':
                           return 'TV-14';
                           break;
                       case 'TVMA':
                           return 'TV-MA';
                           break;
                       default:
                           return null;
                           break;
                    }
                }
            }())
        };
    });
};

var mapDupes = function(shows){

    return shows.reduce(function(collection, show) {
            
            //if the collection contains the show already
            if (_.findWhere(collection, {id: show.id })) {
                // add this airings info the the airings
                _.findWhere(collection, {id: show.id }).airings.push({
                    logo: show.logo,
                    callSign: show.callSign,
                    channel: show.channel
                })

            } else {
                // otherwise, add an airing array and add the first airing.
                show.airings = [];
                show.airings.push({
                    logo: show.logo,
                    callSign: show.callSign,
                    channel: show.channel
                });
                
                collection.push(show);
            }
            
          return collection;
        }, []);
}

module.exports =  function(req, res) {
        
        // if no timestamp is passed, use the rounded timestamp, 
        // aka, the current time rounded down tot he nearest half hour
        var startTime = req.query.startTime; //|| timestampRounder.round();
        
        debug('startTime', startTime)
        var startTimeHour = moment(startTime * 1000).format('HH');
        var offset = parseInt(startTimeHour, 10) + 7;

        debug('startTimeHour', startTimeHour);
        debug('offset', offset);

        var payload = {
            lineupId: req.query.lineupId || 'USA-DFLTE',
            startDateTime: moment.utc(startTime * 1000).format('YYYY-MM-DDTHH:mm') + 'Z'
            // startDateTime: moment.utc(startTime * 1000).format('YYYY-MM-DDT' + offset + ':00') + 'Z'
        };

        if (req.query.endTime) {
            payload['endDateTime'] = moment.utc(req.query.endTime * 1000).format('YYYY-MM-DDTHH:mm') + 'Z'
        }

        debug('newTonight payload', payload);

        api.shows.newTonight(payload)
            .then(transformShows)
            .then(mapDupes)
            .then(startAtPrimetime.bind(null, startTime))
            .then(res.ok)
            .catch(console.log.bind(console));

};