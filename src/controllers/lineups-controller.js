'use strict';

var Gracenote = require('node-tms-onconnect');
var debug = require('debug')('zap2it');
var api = new Gracenote(process.env.API_KEY);
var _ = require('lodash');

var getTZinCanada = require('../shared/pc2timezone');
var getTZinUS = require('../shared/zip2timezone');

module.exports = {

    find: function(req, res){

        var country = req.query.countryCode && req.query.countryCode.toUpperCase();
        var zip = req.query.postalCode;

        function sortAlpha(data){
            return _.sortBy(data, function(item) {
                return item.name;
            });
        }

        // guess the timezone of the returned lineup.
        function decorateWithTimezone(data) {

            return data.map(function (result) {
                
                if (country === "CAN") {
                   result.timezone = getTZinCanada(zip);
                } else {
                   result.timezone = getTZinUS(zip);
                }

                result.isDefaultProvider = false;
                result.postalCode = zip;
                
                return result;
            });
        }

        // seperate lineups by provider type
        function groupByType(data){
            return _.groupBy(data, 'type');
        }

        var payload = {
            country: country || "USA",
            postalCode: zip || "10001"
        };

        api.lineups.find(payload)
            .then(sortAlpha)
            .then(decorateWithTimezone)
            .then(groupByType)
            .then(res.ok).catch(console.log.bind(console));
    },


    channels: function(req, res) {

        var lineupId = req.query.lineupId;

        api.lineups.channels(lineupId)
            .then(res.ok).catch(console.log.bind(console));
    }

};
