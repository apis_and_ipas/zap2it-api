'use strict';

var Gracenote = require('node-tms-onconnect');
var moment = require('moment');
var debug = require('debug')('zap2it');
var api = new Gracenote(process.env.API_KEY);
var _ = require('lodash');
var THREE_HOURS = 60 * 60 * 3;
var setIsDefaultProvider = require('../shared/isDefaultProvider');

module.exports = function (req, res) {

    var lineupId         = req.query.lineupId;
    var startTime        = req.query.time;
    var timespan         = req.query.timespan || 3;
    var excludeChannels  = req.query.excludeChannels || 'music,ppv';
    var timespan_in_mins = 60 * 60 * timespan;
    var endTime          = parseInt(timespan_in_mins, 10) +  parseInt(startTime, 10);

    var cache_key        = lineupId + ':' + startTime + ':' + timespan  + ':' + excludeChannels;
    
    // env vars are ALWAYS strings
    var cacheClient      = req.app.get('cache');
    var isDefaultProvider = setIsDefaultProvider(lineupId);

    var payload = {
        startDateTime : moment.utc(startTime * 1000).format('YYYY-MM-DDTHH:mm') + 'Z', // convert timestamp to GMT time
        endDateTime : moment.utc(endTime * 1000).format('YYYY-MM-DDTHH:mm') + 'Z', // convert timestamp to GMT time
        excludeChannels: excludeChannels
    };


    cacheClient.getAsync(cache_key)
        .then(JSON.parse)
        .then(function(cachedData){

        if (!cachedData) {
            debug('cache miss!', cache_key);

            // hit the Gracenote API
            api.lineups.grid(lineupId, payload)
                .then(processResults) // format response
                .then(function(data) {
                    // cache the data returned from the API, post-processing
                    
                    return cacheClient
                        .setAsync(cache_key, JSON.stringify(data))
                        .then(function(){
                            // Set expiry value for the key
                            return cacheClient.expireAsync(cache_key, THREE_HOURS)
                                .then(function () {
                                    return data;
                                });
                        });
                })
                .then(res.ok)                      // send response
                .catch(console.log.bind(console)); // log any caught errors

        }  else {// served cached data..
            debug('cached response!');
            res.ok(cachedData);
        }
    });

};


/**
* Method use to map all the raw data to usable attributes within the grid.
* @param  {array} data [description]
* @return {[type]}      [description]
*
* @todo Move all these mapping functions into their own module
* @todo It would be nice to get ride of this need/use of this nested `results` object
*/
var processResults = function(channels) {
    return {
        channels: processChannels(channels)
    };
}

 /**
  * Process the channels of course.
  * @param  {array} channels
  * @return {array}
  */
var processChannels = function(channels) {
    return channels.map(function(channel, index) {

        // var callSign = channel.affiliateCallSign ? channel.affiliateCallSign : channel.callSign
        return {
            callSign: channel.callSign,
            affiliateCallSign: channel.affiliateCallSign ? channel.affiliateCallSign : null,
            channelId: channel.stationId,
            channelNo:  parseInt(channel.channel, 10),
            events: processShows(channel.airings, channel.callSign, channel.channel),
            id: channel.stationId + index,
            stationGenres:  processStationGenresFromShows(channel.airings),
            stationFilters:  processStationFiltersFromShows(channel.airings),
            thumbnail: "http://zap2it.tmsimg.com/" + channel.preferredImage.uri + "?w=55"
        };
    })
};


 var getDetailThumbnail = function (show) {
     return "http://zap2it.tmsimg.com/" + show.program.preferredImage.uri + "?w=165";
 };

 /**
 * Maps the data for each show item
 * @param  {array}  shows     
 * @param  {string} callSign  
 * @param  {number} channelNo 
 * @return {array}           
 */
var processShows = function(shows, callSign, channelNo) {

    shows = shows.map(function(show) {
        return {
            callSign: callSign,
            duration: show.duration,
            startTime: show.startTime,
            endTime: show.endTime,
            thumbnail: getDetailThumbnail(show),
            entityType: show.program.entityType,
            channelNo: parseInt(channelNo, 10),
            filter: getShowFilters(show),
            seriesId: show.program.seriesId || null,
            rating: (function () {
                // return desired formatting for rating
                if (show.ratings && show.ratings[0] && show.ratings[0].code) {
                    switch (show.ratings[0].code) {
                        case 'TVY':
                        case 'TVY7':
                        case 'TVY7FV':
                            return 'TV-Y';
                            break;
                        case 'TVG':
                            return 'TV-G';
                            break;
                        case 'TVPG':
                            return 'TV-PG';
                            break;
                        case 'TV14':
                            return 'TV-14';
                            break;
                        case 'TVMA':
                            return 'TV-MA';
                            break;
                        default:
                            return null;
                     }
                 }
             }()),
             flag: (function () {

                 if ( _.includes( show.qualifiers, "Live" ) ) {
                     return "Live";
                 }

                 if ( _.includes( show.qualifiers, "New" ) ) {
                     return "New";
                 }

             })(),
             tags: (function () {
                var tags = [];

                if ( _.includes( show.qualifiers, "Stereo" ) ) {
                     tags.push("Stereo");
                }

                if ( _.includes( show.qualifiers, "CC" ) ) {
                     tags.push("CC") ;
                }

                return tags;
            })(),
            program: {
                title:        show.program.title || null,
                id:           show.program.tmsId || null,
                tmsId:        show.program.tmsId || null,
                shortDesc:    show.program.shortDescription || null,
                season:       show.program.seasonNum    || null,
                releaseYear:  show.program.releaseYear   || null,
                episode:      show.program.episodeNum   || null,
                episodeTitle: show.program.episodeTitle || null,
                seriesId:     show.program.seriesId || null
            }
        }
    });

    // sometime shows with identical start/end times show up, filter those out
    return _.uniq(shows, function(show) {
        return show.startTime;
    });
};

/**
* Takes and array of shows and returns array of all the genres for of all the shows
* Use for filtering by content type
* @param  {array} shows
* @return {array} genres
*/
var processStationGenresFromShows = function(shows) {
    return _(shows.map(function(show) {
        return _.isArray(show.program.genres) && show.program.genres.map(function(genre){
            return genre.toLowerCase();
        });
    }))
    .flatten()
    .unique()
    .value()
};


var processStationFiltersFromShows = function(shows) {
    return _(shows.map(function(show) {
        return getShowFilters(show);
    }))
    .flatten()
    .unique()
    .value()
};


var getShowFilters = function(show) {
    var showFilters = [];

    // Family
    if ( _.includes(show.program.genres, "Children") ||
        show.program.audience && show.program.audience === "Children") {
        showFilters.push("filter-family");
    }

    // Movies
    if ( show.program.entityType === "Movie" ) {
        showFilters.push("filter-movie");
    }

    // News
    if ( _.includes(show.program.genres, "News") ) {
        showFilters.push("filter-news");
    }

    // Sports
    if ( show.program.entityType === "Sports" 
      || show.program.subType === "Sports event" 
      || show.program.subType === "Sports non-event" 
      || _.includes(show.program.genres, "Sports talk") ) {
        showFilters.push("filter-sports");
    }

    // Talk
    if ( _.includes(show.program.genres, "Talk") ) {
        showFilters.push("filter-talk");
    }

    return showFilters;
};
 

