/**
 * Maps a Canada postal code to a (marginal) guess at your Timezone.
 * @type {Object}
 */
var can_data = {
  'America/Chicago': [
    'S', 'R', 'X0C'
  ],
  'America/New_York': [
    'J', 'G', 'H', 'L', 'K', 'M', 'N', 'X0A'
  ],
  'America/Denver': [
    'T', 'X0B'
  ],
  'America/Los_Angeles': [
    'V', 'Y'
  ],
  'America/Halifax': [
    'B', 'C', 'E'
  ],
  'America/St_Johns': [
    'A'
  ],
  unused: [
    'D' , 'F' , 'I' , 'O', 'Q', 'U', 'W', 'Z'
  ],
  zone_names: {
    'America/Los_Angeles': "Pacific Time (US & Canada)",
    'America/Denver': "Mountain Time (US & Canada)",
    'America/Chicago': "Central Time (US & Canada)",
    'America/New_York': "Eastern Time (US & Canada)",
    'America/Halifax': "Atlantic Time (US & Canada)",
    'America/St_Johns': "Newfoundland Time (US & Canada)"
  },
  zones: [ 'America/Halifax', 'America/St_Johns' , 'America/Chicago', 'America/New_York', 'America/Denver', 'America/Los_Angeles', 'unused']
};


var mapping = {};

can_data.zones.forEach(function (key) {
  can_data[key].forEach(function (zip) {
    mapping[zip] = (key == 'unused') ? null : key;
  })
});


module.exports = function (zip) {

     if (!(typeof zip == 'string' || zip instanceof String)) {

        console.log('pc2timezone: This function only accepts strings. Please correct this.');
        console.log(zip);

        return false;
    }
    for (var i = 6; i >= 0; i--) {
        zip = zip.replace(' ', '').toUpperCase(); // remove optional space and normalize case
        var checkString = zip.substr(0, i);
        if (mapping[checkString]) {
            return mapping[checkString]
        }
    }
    return "unknown";
};;
