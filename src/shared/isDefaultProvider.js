var _ = require('lodash');

var defaultIds = [
    "USA-DFLTP", // America/Los_Angelos
    "USA-DFLTM", // America/Denver
    "USA-DFLTC", // America/Chicago
    "USA-DFLTA", // America/Anchorage
    "USA-DFLTH", // America/Honululu
    "USA-DFLTE"  // America/New_York
];

/**
 * Is our lineupId present in the defaultIds array?
 * @param  {lineupId} lineupId 
 * @return {Boolean} 
 */
module.exports = function(lineupId){
    return _.contains(defaultIds, lineupId);
};