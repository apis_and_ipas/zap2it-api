var roundedTimestamp = require('./round-unix-timestamp');

/**
 * @param  {String} lineupId 
 * @return {String}          cache key based on the lineupId and a unix timestamp rounded down to the last half houw
 */
module.exports = function(lineupId){
    if (!lineupId) return;
    return lineupId + '_' + roundedTimestamp();
}