var Gracenote = require('node-tms-onconnect');
var api = new Gracenote(process.env.API_KEY);
var Promise = require('bluebird');
var redis     = require('redis');
var cacheClient = Promise.promisifyAll(redis.createClient());
var _ = require('lodash');
var ONE_DAY = 60 * 60 * 24;
var ONE_MONTH = ONE_DAY * 30;


/**
 * Fetch show detail given a show's tmdId
 * @param  {String} tmsId  
 */
var fetchDetails = function(tmsId) {
    var cache_key = 'showDetails:' + tmsId;

    return cacheClient.getAsync(cache_key)
        .then(JSON.parse)
        .then(function(cachedData){

            if (!cachedData) {
                // fetch showdata from TMS API
                api.shows.details(tmsId)
                    .then(function(data) {
                        return cacheClient
                            .setAsync(cache_key, JSON.stringify(data))
                            .then(function() {
                                return cacheClient.expireAsync(cache_key, ONE_MONTH)
                                    .then(function() {
                                        return data;
                                    });
                            });
                    });

            } else {
                return cachedData;
            }

        })
        .catch(console.log.bind(console))


};



/**
 * Fetch show airings given a show's seriesId
 * @param  {String} seriesId  
 * @param  {Object} params 
 */
var fetchAirings = function(seriesId, params) {
    var cache_key = 'showAirings:' + seriesId + ':' + params.lineupId;
    var use_cache = process.env.USE_CACHE || true;

    return cacheClient.getAsync(cache_key)
        .then(JSON.parse)
        .then(function(cachedData){

            if (!cachedData) {
                // fetch showdata from TMS API
                return api.series.airings(seriesId, params)
                    .then(processAirings)
                    .then(function(data) {
                        return cacheClient
                            .setAsync(cache_key, JSON.stringify(data))
                            .then(function() {
                                return cacheClient.expireAsync(cache_key, ONE_DAY)
                                    .then(function() {
                                        return data;
                                    });
                            });
                    }); 

            } else {
                return cachedData;
            }

        })
        .catch(console.log.bind(console))

};

var groupAiringsByTime = function(airings) {
    return _.map(airings, function(airing){
        return airing;
    });
};


var processAirings = function(airings){
    // no airings, no go.
    if (!(airings.length > 0)) return airings;

    var episodeMap = {};

    var show = {
        tmsId: airings[0] && airings[0].program && airings[0].program.tmsId,
        title: airings[0] && airings[0].program && airings[0].program.title,
        seriesId: airings[0] && airings[0].program && airings[0].program.seriesId,
        logo: (airings[0] && airings[0].program && airings[0].program.preferredImage && airings[0].program.preferredImage.uri) ? ("http://zap2it.tmsimg.com/" + airings[0].program.preferredImage.uri + "?w=60") : null
    };

    _.each(airings, function(airing){
        var episodeIdentifier = airing && airing.program && airing.program.episodeTitle || "Untitled";
        // if an Episode title isnt in the list, add it.
        if ( !_.has(episodeMap, episodeIdentifier) ) {
            episodeMap[episodeIdentifier] = {
                episodeTitle: episodeIdentifier,
                episodeNum: airing.program && airing.program.episodeNum,
                seasonNum: airing.program && airing.program.seasonNum,
                description: airing.program && airing.program.longDescription,
                rating: (function () {
                     // return desired formatting for rating
                     if (airing.ratings && airing.ratings[0] && airing.ratings[0].code) {
                        switch (airing.ratings[0].code) {
                            case 'TVY':
                            case 'TVY7':
                            case 'TVY7FV':
                                    return 'TV-Y';
                            case 'TVG':
                                return 'TV-G';
                                break;
                            case 'TVPG':
                                return 'TV-PG';
                                break;
                            case 'TV14':
                                return 'TV-14';
                                break;
                            case 'TVMA':
                                return 'TV-MA';
                                break;
                            default:
                                return null;
                                break;
                        }
                     }
                }()),
                flag: (function () {

                     if ( _.includes( airing.qualifiers, "Live" ) ) {
                         return "Live";
                     }

                     if ( _.includes( airing.qualifiers, "New" ) ) {
                         return "New";
                     }

                 })(),
                 tags: (function () {
                    var tags = [];

                    if ( _.includes( airing.qualifiers, "Stereo" ) ) {
                         tags.push("Stereo");
                    }

                    if ( _.includes( airing.qualifiers, "CC" ) ) {
                         tags.push("CC") ;
                    }

                    return tags;
                })(),
                airings: {},
                isNew: false // default all episodes to not be new..
            };
        }

        // qualify an episode as new if even a single airing is new, 
        // for our purposes: 'if you havent seen it, it new to you'
        // 
        if ( _.includes( airing.qualifiers, "New" ) ) {
           episodeMap[episodeIdentifier].isNew = true;
        }

        // if an episodes start time isnt it's respecitive episodes list of start time, add it.
        if ( !episodeMap[episodeIdentifier]['airings'][airing.startTime] ) {
            episodeMap[episodeIdentifier]['airings'][airing.startTime] = [];
        }

        // add airing details to the array of airings at this startStart
        var logoUrl = 
            (airing && airing.station 
                && airing.station.preferredImage 
                && airing.station.preferredImage.uri) 
            ? "http://zap2it.tmsimg.com/" + airing.station.preferredImage.uri + "?w=60"
            : null;

        episodeMap[episodeIdentifier]['airings'][airing.startTime].push({
            channel: airing.station && airing.station.channel,
            callSign: airing.station && airing.station.callSign,
            logo:  logoUrl
        });

        // re-sort airings by channel number
        episodeMap[episodeIdentifier]['airings'][airing.startTime].sort(function(a, b){
            return parseInt(a.channel, 10) - parseInt(b.channel, 10);
        })
        
    });

    // attach episode show object
    show.episodes = episodeMap;

    return show;
};

     

module.exports = {
    fetchDetails: fetchDetails,
    fetchAirings: fetchAirings
};