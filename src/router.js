var express         = require('express'),
    cache           = require('express-redis-cache')(),
    lineupCtrl      = require('./controllers/lineups-controller'),
    newTonightCtrl  = require('./controllers/new-tonight-controller'),
    gridCtrl        = require('./controllers/grid-controller'),
    myShowsCtrl     = require('./controllers/my-shows-controller'),
    showsCtrl       = require('./controllers/shows-controller')
    app             = require('../app').app;

var ONE_DAY = 60 * 60 * 24;
var FIFTEEN_DAYS = ONE_DAY * 15; 


/**
 * Sets a routes caching parameters, if the cache is enabled
 * @param  {String} ttl time til cache expires for this route.
 * @return {Function}   middleware funciton
 */
var maybeCache = function(ttl){
    return function(req, res, next){
        var use_cache = process.env.USE_CACHE;
        use_cache = (use_cache === "true"); // convert from String to Boolean
        if (use_cache) {
            cache.route({ expire: ttl })
        }
        next();
    }
}

 
module.exports = function(){

    var router = express.Router();

    router.get('/lineups/find',            maybeCache(FIFTEEN_DAYS), lineupCtrl.find);
    router.get('/lineups/channels',        maybeCache(FIFTEEN_DAYS), lineupCtrl.channels);
    router.get('/shows/newTonight',        maybeCache(ONE_DAY),      newTonightCtrl);
    router.get('/shows/:tmsId',            maybeCache(FIFTEEN_DAYS), showsCtrl.details);
    router.get('/seriesAirings/:seriesId', maybeCache(FIFTEEN_DAYS), showsCtrl.airings);

    //cacheing done manually on these routes
    router.get('/grid', gridCtrl); 
    router.get('/my-shows', myShowsCtrl); 

    router.get('/status', function(res, res) {
        res.send("OK");
    });

    return router;
};
