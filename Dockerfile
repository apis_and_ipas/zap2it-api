FROM ubuntu:14.04
MAINTAINER dan@doejo.com

# Fix locale issues
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN touch /etc/default/locale && \
    echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale && \
    echo "LANG=en_US.UTF-8" >> /etc/default/locale

# Update system and install necessary dependencies
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y ca-certificates openssl wget libssl-dev libssl1.0.0

# Install stable nodejs runtime
RUN wget -q http://nodejs.org/dist/v0.10.38/node-v0.10.38-linux-x64.tar.gz && \
    tar -zxf node-v0.10.38-linux-x64.tar.gz && \
    cp -a node-v0.10.38-linux-x64/bin/* /usr/local/bin/ && \
    cp -a node-v0.10.38-linux-x64/include/* /usr/local/include/ && \
    cp -a node-v0.10.38-linux-x64/lib/* /usr/local/lib/ && \
    cp -a node-v0.10.38-linux-x64/share/* /usr/local/share/ && \
    rm -rf node-v0.10.38*

# Install redis
RUN apt-get install -y redis-server
RUN sed 's/^daemonize yes/daemonize no/' -i /etc/redis/redis.conf

# Install and configure supervisor
RUN apt-get install -y supervisor
ADD docker/supervisor/supervisord.conf /etc/supervisor/supervisord.conf

# Cleanup!
RUN rm -rf /tmp/*
RUN for i in /var/log/*.log; do cat /dev/null > $i; done

# Setup application
ADD package.json /app/package.json
WORKDIR /app
RUN npm install
ADD . /app

# Checkout revision and remove git
RUN cat .git/refs/heads/master > REVISION && rm -rf .git

# Expose nginx port only
EXPOSE 80

# Start supervisor by default
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]